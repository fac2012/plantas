/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package udem.edu.co.controller;

import java.util.ArrayList;

/**
 *
 * @author FAYDER CUERVO
 */
public class MainPlantas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        ArrayList <PlantasController> lista = new ArrayList<>();
        
        PlantasController planta1 = new CarnivoraController("a", 5, "Arbol", 0);
        PlantasController planta2 = new CarnivoraController("w", 6, "Hierva", 0);
        PlantasController planta3 = new NoCarnivoraController("e", 3, "Arbol", 0);
        PlantasController planta4 = new CarnivoraController("r", 20, "Arbusto", 0);
        PlantasController planta5 = new NoCarnivoraController("y", 10, "Hierva", 0);
        PlantasController planta6 = new NoCarnivoraController("f", 15, "Arbusto", 0);
        PlantasController planta7 = new CarnivoraController("d", 11, "Arbol", 0);
        PlantasController planta8 = new NoCarnivoraController("g", 8, "Hierva", 0);
        
        lista.add(planta1);
        lista.add(planta2);
        lista.add(planta3);
        lista.add(planta4);
        lista.add(planta5);
        lista.add(planta6);
        lista.add(planta7);
        lista.add(planta8);
         
        for (int i = 0; i < lista.size(); i++) {
            System.out.println(lista.get(i).toString());
        }
        
        
    }
    
}
