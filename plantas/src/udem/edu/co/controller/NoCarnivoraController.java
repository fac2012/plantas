/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package udem.edu.co.controller;

/**
 *
 * @FAYDER CUERVO
 */
public class NoCarnivoraController extends PlantasController{
    
    public NoCarnivoraController(String nombre, int edad, String tipo, float promedio_vida) {
        super(nombre, edad, tipo, promedio_vida);
    }
    
    @Override
    public String toString() {
        return "No carnivora{" + "nombre= " + getNombre() + ", edad= " + getEdad() + " ,tipo= " + getTipo() + ", Promedio de vida= " + getPromedio_vida() + '}';
    }
}
