/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package udem.edu.co.controller;


/**
 *
 * @author FAYDER CUERVO
 */
public class PlantasController {
    
    private String nombre;
    private int edad;
    private String tipo;
    private float promedio_vida;

    public PlantasController(String nombre, int edad, String tipo, float promedio_vida) {
        this.nombre = nombre;
        this.edad = edad;
        this.tipo = tipo;
        this.promedio_vida = promedio_vida;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public float getPromedio_vida() {
        return promedio_vida;
    }

    public void setPromedio_vida(float promedio_vida) {
        this.promedio_vida = promedio_vida;
    }
    
    public void mostrarCarnivoras(){
        
    }
    
    
}
