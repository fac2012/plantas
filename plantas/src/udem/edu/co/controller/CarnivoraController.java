/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package udem.edu.co.controller;

/**
 *
 * @author FAYDER CUERVO
 */
public class CarnivoraController extends PlantasController{

    public CarnivoraController(String nombre,int edad,String tipo,float promedio_vida) {
        super(nombre,edad,tipo,promedio_vida);
    }

    @Override
    public String toString() {
        return "carnivora{" + "nombre= " + getNombre() + ", edad= " + getEdad() + " ,tipo= " + getTipo() + ", Promedio de vida= " + getPromedio_vida() + '}';
    }
    
}
